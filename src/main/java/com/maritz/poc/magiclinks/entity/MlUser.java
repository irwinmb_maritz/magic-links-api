package com.maritz.poc.magiclinks.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name="ml_user")
public class MlUser implements Serializable {
	
	private static final long serialVersionUID = 5644444517604335684L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@NotEmpty(message="Username is required")
	@Column(nullable=false)
	private String username;
	
	@NotEmpty(message="First Name is required")
	@Column(nullable=false)
	private String firstName;
	
	@NotEmpty(message="Email is required")
	@Column(nullable=false)
	private String email;
	
	@NotEmpty(message="Password is required")
	@Column(nullable=false)
	private String password;
	
	@Column(nullable=true)
	private String phone;
	
	@Column(nullable=true)
	private String deviceId;
	
	

}
