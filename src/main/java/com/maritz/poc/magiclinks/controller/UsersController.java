package com.maritz.poc.magiclinks.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.poc.magiclinks.entity.MlUser;
import com.maritz.poc.magiclinks.repository.MlUserRepository;

@RestController
@CrossOrigin(origins="*")
public class UsersController {

	private MlUserRepository mlUserRepository;
	
	@Autowired
	public UsersController(MlUserRepository mlUserRepository) {
		this.mlUserRepository = mlUserRepository;
	}
	
	@GetMapping("/users")
	public List<MlUser> getAllUsers() {
		return mlUserRepository.findAll();
	}
	
	@GetMapping("/users/{id}")
	public MlUser getUserById(@PathVariable("id") long id) {
		return mlUserRepository.getById(id);
	}
	
	@PostMapping("/users")
	public MlUser createUser(@Valid @RequestBody MlUser user) {
		return mlUserRepository.save(user);
	}

	@GetMapping("/users/findByEmail/{emailAddress}")
	public MlUser getUserById(@PathVariable("emailAddress") String emailAddress) {
		return mlUserRepository.findByEmail(emailAddress).get();
	}
	

}
