package com.maritz.poc.magiclinks.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.poc.magiclinks.dto.VerificationCheckDto;
import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;

@RestController
@CrossOrigin(origins="*")
public class MagicLinkController {
	
	private final String verifyServiceSid;
	
	public MagicLinkController(
			@Value("${TWILIO_ACCOUNT_SID:AC66b43eafbb5116e2cf9def28a4b3a6b0}") String accountSid,
			@Value("${TWILIO_AUTH_TOKEN:651bb037f50c6a9fdf89bf308ef5006f}") String authToken,
			@Value("${TWILIO_VERIFY_SID:VAcf51c6465e7ca1778c2c12e7b33972e3}") String verifyServiceSid
			) {
		super();
		Twilio.init(accountSid, authToken);
		this.verifyServiceSid = verifyServiceSid;
	}

	@PostMapping("/magic-link/{emailAddress}")
	public Verification createMagicLink(@PathVariable("emailAddress") String emailAddress) {
		
		Map<String, Object> substitutions = new HashMap<>();
		substitutions.put("email", emailAddress);
		substitutions.put("callback_url", "http://localhost:3000");

		Map<String, Object> channelConfiguration = new HashMap<>();
		channelConfiguration.put("substitutions", substitutions);
		
		Verification verification = Verification
				.creator(this.verifyServiceSid, emailAddress, "email")
				.setChannelConfiguration(channelConfiguration)
				.create();
		return verification;
	}
	
	@PutMapping("/magic-link")
	public VerificationCheck verifyMagicLink(@RequestBody VerificationCheckDto verificationCheckDto) {
		VerificationCheck verificationCheck = VerificationCheck
				.creator(this.verifyServiceSid, verificationCheckDto.getToken())
				.setTo(verificationCheckDto.getTo())
				.create();
		
		return verificationCheck;
		
	}

}
