package com.maritz.poc.magiclinks.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maritz.poc.magiclinks.entity.MlUser;

@Repository
public interface MlUserRepository extends JpaRepository<MlUser, Long> {
	
	Optional<MlUser> findByEmail(String email);

}
