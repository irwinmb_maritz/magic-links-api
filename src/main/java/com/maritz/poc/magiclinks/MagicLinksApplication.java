package com.maritz.poc.magiclinks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude=SecurityAutoConfiguration.class)
public class MagicLinksApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagicLinksApplication.class, args);
	}

}
